// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name : "Apple",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
  },

  {
    name : "Banana",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
  },

  {
    name : "Kiwi",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
  },

  { 
    name : "Mango",
    stock : 10,
    price: 20,
    supplier_id : 2,
    onSale : false,
  }
]);



/*-------------------------------------------------------------------*/

// ACTIVITY


// Result of MongoDB Aggregation to count the total number of fruits on sale.
/* Count operator method */
db.fruits.aggregate([
  {$match: {onSale: true} },
  {$count: "fruitsOnSale" }
]);


/* Sum + project method */
db.fruits.aggregate([
  {$match: {onSale: true} },
  {$group: {_id: "$onSale", fruitsOnSale: {$sum: 1} } },
  {$project: {_id: 0}}
]);





// Result of MongoDB Aggregation to count the total number of fruits with stock more than 20.

db.fruits.aggregate([
  {$match: {stock: {$gte: 20}} },
  {$count: "enoughStock" }
])






// Result of MongoDB Aggregation to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
  {$match: {onSale: true }},
  {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"} } },
]);




// Result of MongoDB Aggregation to get the highest prie of a fruit per supplier.

db.fruits.aggregate([
  {$match: {stock: {$gte: 10} }},
  {$group: {_id: "$supplier_id", max_price: {$max: "$price"} } }
]);




// Result of MongoDB Aggregation to get the lowerst price of a fruit per supplier.

db.fruits.aggregate([
  {$match: {stock: {$gte: 10} }},
  {$group: {_id: "$supplier_id", min_price: {$min: "$price"} } }
]);